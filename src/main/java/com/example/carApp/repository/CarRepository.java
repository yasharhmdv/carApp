package com.example.carApp.repository;

import com.example.carApp.carModel.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
}
