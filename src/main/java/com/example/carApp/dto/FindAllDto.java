package com.example.carApp.dto;

import com.example.carApp.carModel.Car;

import java.util.List;

public class FindAllDto {
    String modelName;
    String color;
    Double engine;
    String date;

}
