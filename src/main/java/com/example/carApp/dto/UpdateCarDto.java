package com.example.carApp.dto;

import jakarta.persistence.Entity;
import lombok.Data;

@Data
public class UpdateCarDto {
    Integer id;
    String modelName;
    String color;
    Double engine;
    String date;
}
