package com.example.carApp.dto;

import lombok.Data;

@Data
public class CreateCarDto {

    String modelName;
    String color;
    Double engine;
    String date;
}
