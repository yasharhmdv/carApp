package com.example.carApp.dto;

import lombok.Data;

@Data
public class CarDto {
    Integer id;
    String modelName;
    String color;
    Double engine;
    String date;
}
