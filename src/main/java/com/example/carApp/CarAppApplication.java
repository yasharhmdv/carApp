package com.example.carApp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarAppApplication {

	@Value("${ms.name")
	private String name;

	public static void main(String[] args) {
		SpringApplication.run(CarAppApplication.class, args);
	}

	public void run(String... args)throws Exception{
		System.out.println("Name is: "+ name);
	}

}
