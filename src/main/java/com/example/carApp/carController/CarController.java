package com.example.carApp.carController;

import com.example.carApp.carModel.Car;
import com.example.carApp.dto.CarDto;
import com.example.carApp.dto.CreateCarDto;
import com.example.carApp.dto.UpdateCarDto;
import com.example.carApp.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto car){

        carService.create(car);

    }
    @PutMapping
    public void update(@RequestBody UpdateCarDto car){

        carService.update(car);
    }
    @GetMapping("/{id}")
    public CarDto get(@PathVariable Integer id){

        return carService.get(id);
    }
    @GetMapping
    public List<CarDto> geAll(){
        return carService.getAll();
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){

        carService.delete(id);
    }

}
