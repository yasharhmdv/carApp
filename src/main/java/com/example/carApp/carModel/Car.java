package com.example.carApp.carModel;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Car {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     Integer id;
     String modelName;
     String color;
     Double engine;
     String date;

}
