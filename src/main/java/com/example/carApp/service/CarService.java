package com.example.carApp.service;

import com.example.carApp.carModel.Car;
import com.example.carApp.dto.CarDto;
import com.example.carApp.dto.CreateCarDto;
import com.example.carApp.dto.FindAllDto;
import com.example.carApp.dto.UpdateCarDto;
import com.example.carApp.repository.CarRepository;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CarService {

    private final CarRepository carRepository;
    private final ModelMapper modelMapper;

    public CarService(CarRepository carRepository, ModelMapper modelMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }

    public void create(CreateCarDto dto) {
        /*Car car = new Car();
        car.setModelName(dto.getModelName());
        car.setColor(dto.getColor());
        car.setDate(dto.getDate());
        car.setEngine(dto.getEngine());*/
        Car car = modelMapper.map(dto,Car.class);

        carRepository.save(car);
    }

    public void update(UpdateCarDto dto) {
        Optional<Car> entity = carRepository.findById(dto.getId());
        entity.ifPresent(car1 -> {
            if (dto.getModelName() != null){
            car1.setModelName(dto.getModelName());}
            if (dto.getColor() != null){
            car1.setColor(dto.getColor());}
            if (dto.getEngine() != null){
            car1.setEngine(dto.getEngine());}
            if (dto.getDate() != null){
            car1.setDate(dto.getDate());}

            carRepository.save(car1);
        });
    }

    public CarDto get(Integer id) {
        Car car = carRepository.findById(id).get();
        /*CarDto carDto = new CarDto();
        carDto.setModelName(car.getModelName());
        carDto.setColor(car.getColor());
        carDto.setEngine(car.getEngine());
        carDto.setDate(car.getDate());*/
        CarDto carDto = modelMapper.map(car,CarDto.class);
        return carDto;
    }
    public List<CarDto> getAll(){
        List<Car> carList = carRepository.findAll();
        List<CarDto> carDtoList = carList
                .stream()
                .map(car-> modelMapper.map(car,CarDto.class))
                .collect(Collectors.toList());
        return carDtoList;

    }

    public void delete(Integer id) {

        carRepository.deleteById(id);
    }
}
